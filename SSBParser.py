import csv

info_list, int_list = [], []

def parse(filename):
    with open(filename, encoding='ISO-8859-1') as csvfile:
        parser = csv.reader(csvfile, delimiter=';')
        for line in parser:
            if line:
                for i in range(len(line)):
                    if len(line[i]) < 10:
                        if line[i] != ' ':
                            try:
                                int_list.append(int(line[i]))
                            except:
                                continue # skip needless data
                    else:
                        info_list.append(line[i])
    return(info_list, int_list[:18-1], int_list[18-1:])

def emptyLists():
    del info_list[:]
    del int_list[:]
