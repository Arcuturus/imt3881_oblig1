def trapezoidal(a, b, n, lst):
    h = float(b-a)/n
    result = 0.5*lst[a] + 0.5*lst[b-1]
    for i in range(1, n-1):
        result += lst[i]
    return result / h
