import matplotlib.pyplot as plt
from SSBParser import parse, emptyLists
from trapezoidal import trapezoidal
import numpy as np

years      = []
f_students = []
m_students = []
f_info     = []
m_info     = []
average    = []

year_as_int = []

# Get data about male students
m_info, years, m_students = parse('ssb_menn.csv')
emptyLists() # clear out the lists

# Get data about female students
f_info, years, f_students = parse('ssb_kvinner.csv')
emptyLists() # clear out the lists

for i in range(len(years)):
    average.append((f_students[i] + m_students[i]) / 2)

f_trapezoid = trapezoidal(0, len(f_students), len(f_students), f_students)
print(f_trapezoid)
print(np.trapz(f_students))

# Generate a new int list based on years to be used in barplot
for i in range(len(years)):
    year_as_int.append(int(years[i]))

f_students_mean = [np.mean(f_students) for f in f_students]
m_students_mean = [np.mean(m_students) for m in m_students]

data, = plt.plot(years, f_students, label="female Students")
data.set_color("black")
line, = plt.plot(years, f_students_mean, label="mean value", linestyle="--")
line.set_color("orange")
plt.xlabel("Year")
plt.ylabel("Number of students")
plt.title("Number of female students in science, craft and technology")
plt.legend(loc="lower right")


# Drawing the graphs in a 2x2
# plt.subplot(221)
# plt.plot(years, m_students, label="Number of Male Students")
# line, = plt.plot(years, average, label="Average", color="red")
# line.set_color("red")
# plt.legend()
#
# plt.subplot(222)
# plt.plot(years, f_students, label="Number of Female Students")
# line, = plt.plot(years, average, label="Average", color="red")
# line.set_color("red")
# plt.legend()
#
# plt.subplot(223)
# plt.bar(year_as_int, m_students, align="center", label="Male students barplot")
# line, = plt.plot(years, m_students, color="orange")
# line.set_color("orange")
# plt.legend()
#
# plt.subplot(224)
# plt.bar(year_as_int, f_students, align="center", label="Female students barplot")
# line, = plt.plot(years, f_students, color="orange")
# line.set_color("orange")
# plt.legend()
#
# plt.suptitle("Fordeling mellom kvinner og menn i Naturvitenskapelige fag, handverksfag og tekniske fag")
#plt.show()
